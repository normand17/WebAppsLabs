/*
 * task.js
 * name: Dakotah Norman
 * Contains implementation for a "task" "class"
 */

var Task, taskObj, idnum, tags, title, proto, i;

idnum = 0;
// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */

function makeNewTask() {
   idnum += 1;
   taskObj = Object.create(proto);
   taskObj.id = idnum;
   taskObj.title = '';
   taskObj.completedTime = null;
   taskObj.tags = [];
   Object.defineProperty(taskObj, 'id', {
      configurable : false,
      enumerable : true,
      writable : false
   });
   Object.defineProperty(taskObj, 'tags', {
      configurable : false,
      enumerable : false,
      writable : false
   });
   taskObj.prototype = proto;
   // Object.defineProperty(Task, 'prototype', {
   //    value: proto,
   //    writable: false
   // });
   Object.preventExtensions(taskObj);

   return taskObj;

}

function makeTaskFromObject(o) {
   var taskFromObj;

   taskFromObj = Task.new();
   taskFromObj.prototype.setTitle.call(taskFromObj, o.title);
   taskFromObj.prototype.addTags.call(taskFromObj, o.tags);

   return taskFromObj;
}

function makeTaskFromString(str) {
   var o, stringObj;

   o = processString(str);
   stringObj = Task.fromObject.call(Task, o);

   return stringObj;

}


/*
*       Prototype / Instance methods
*/

proto = {
   //Add instance methods here
   setTitle : function(s) {
      this.title = s.trim();

      return this;
   },
   isCompleted : function() {
      if (this.completedTime !== null) {

         return true;
      }

      return false;
   },
   toggleCompleted : function() {
      if (this.prototype.isCompleted.call(this) === true) {
         this.completedTime = null;

         return this;
      }
      taskObj.completedTime = new Date();

      return this;
   },
   hasTag : function(tag) {
      if (this.tags.indexOf(tag) > -1) {

         return true;
      }

      return false;
   },
   addTag : function(tag) {
      if (this.prototype.hasTag.call(this, tag) === false) {
         this.tags.length += 1;
         this.tags[this.tags.length - 1] = tag;
      }
   },
   removeTag : function(tag) {
      if (this.prototype.hasTag.call(this, tag) === true) {
         this.tags[this.tags.indexOf(tag)] = undefined;
      }

      return this;
   },
   toggleTag : function(tag) {
      if (this.prototype.hasTag.call(this, tag) === false) {
         this.prototype.addTag.call(this, tag);

         return this;
      }
      this.prototype.removeTag.call(this, tag);

      return this;
   },
   addTags : function(arr) {
      for (i = 0; i < arr.length; i += 1) {
         if (this.prototype.hasTag.call(this, arr[i]) === false) {
            this.prototype.addTag.call(this, arr[i]);
         }
      }

      return this;
   },
   removeTags : function(arr) {
      for (i = 0; i < arr.length; i += 1) {
         if (this.prototype.hasTag.call(this, arr[i]) === true) {
            this.prototype.removeTag.call(this, arr[i]);
         }
      }

      return this;
   },
   toggleTags : function(arr) {
      for (i = 0; i < arr.length; i += 1) {
         this.prototype.toggleTag.call(this, arr[i]);
      }

      return this;
   },
   clone : function() {
      var taskClone;

      taskClone = Task.fromObject.call(Task, this);
      // taskClone.title = this.title;
      taskClone.isCompleted = this.isCompleted;
      // taskClone.tags = this.prototype.addTags.call(this, this.tags);

      return taskClone;
   }

};


// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
