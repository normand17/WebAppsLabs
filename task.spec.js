/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
describe('Your code for Task', function(){
	it('defines a function makeNewTask', function(){
		expect(Task.new).to.be.a('function');
	});

});
describe('Your makeNewTask function', function(){
	var newTask = Task.new();
	it('returns the taskObj object', function(){
		expect(newTask).to.be.a('object');
	});
	//console.log(newTask.id);
	it('id is 1 for the first task', function(){
		expect(newTask.id).to.equal(1);
	});
	it('title is empty for a new task', function(){
		expect(newTask.title).to.equal("");
	});
	var newTask2 = Task.new();
	it('id is 2 for the second task', function(){
		expect(newTask2.id).to.equal(2);
	});
	it('completedTime returns null for a new task', function(){
		expect(newTask.completedTime).to.equal(null);
	});
	console.log(newTask.tags);
	it('tags returns an empty array for a new task', function(){
		expect(newTask.tags).to.deep.equal([]);
	});

});
describe('Your proto function', function(){
	it('setTitle changes the title of the task', function() {
		var newTask = Task.new();
		var str = '  things  ';
		newTask.prototype.setTitle.call(newTask,str);
		expect(newTask.title).to.equal('things');
	});
	it('isCompleted shows the a new task is not complete', function() {
		var newTask = Task.new();
		expect(newTask.prototype.isCompleted.call(newTask)).to.equal(false);
		

	});
	it('toggleCompleted changes the completedTime status', function() {
		var newTask = Task.new();
		newTask.prototype.toggleCompleted.call(newTask);
		console.log(newTask.completedTime);
		newTask.prototype.toggleCompleted.call(newTask);
		expect(newTask.completedTime).to.equal(null);
	});
	it('hasTag shows that a tag is in the tags', function() {
		var newTask = Task.new();
		newTask.tags[newTask.tags.length] = 'here';
		newTask.tags[newTask.tags.length] = 'also here';

		console.log(newTask.tags);
		expect(newTask.prototype.hasTag.call(newTask, 'here')).to.equal(true);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(false);
	});
	it('addTag adds a tag to the tags', function() {
		var newTask = Task.new();
		newTask.prototype.addTag.call(newTask, 'not here');
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		newTask.prototype.addTag.call(newTask, 'here here');
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);
	});
	it('removeTag removes a tag from the tags', function() {
		var newTask = Task.new();
		newTask.prototype.addTag.call(newTask, 'not here');
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		newTask.prototype.addTag.call(newTask, 'here here');
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);

		newTask.prototype.removeTag.call(newTask, 'not here');
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(false);
		newTask.prototype.removeTag.call(newTask, 'here here');
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(false);
	});
	it('toggleTag changes the status of a tag based on the hasTag', function() {
		var newTask = Task.new();
		newTask.prototype.addTag.call(newTask, 'not here');
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		newTask.prototype.addTag.call(newTask, 'here here');
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);

		newTask.prototype.toggleTag.call(newTask, 'not here');
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(false);
		newTask.prototype.toggleTag.call(newTask, 'definitely not here');
		expect(newTask.prototype.hasTag.call(newTask, 'definitely not here')).to.equal(true);
	});
	it('addTags adds all tags from an array', function() {
		var newTask = Task.new();
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);
		console.log(newTask.tags);
	});
	it('removeTags removes all tags that exist in the array', function() {
		var newTask = Task.new();
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);

		var arr2 = ['not here', 'here here'];
		newTask.prototype.removeTags.call(newTask, arr2);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(false);
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(false);
		console.log(newTask.tags);
	});
	it('toggleTags changes status of all tags existing in the array', function() {
		var newTask = Task.new();
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(true);
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(true);

		var arr2 = ['not here', 'here here'];
		newTask.prototype.toggleTags.call(newTask, arr2);
		expect(newTask.prototype.hasTag.call(newTask, 'not here')).to.equal(false);
		expect(newTask.prototype.hasTag.call(newTask, 'here here')).to.equal(false);
	});
	it('clone return the new task that is the same as this', function() {
		var newTask = Task.new();
		newTask.title = 'thing';
		newTask.isCompleted = null;
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);
		console.log(newTask.tags);

		var clonedTask = newTask.prototype.clone.call(newTask);
		expect(clonedTask.title).to.equal(newTask.title);
		console.log(clonedTask.title);
		expect(clonedTask.isCompleted).to.equal(newTask.isCompleted);
		console.log(clonedTask.tags);
		expect(clonedTask.tags).to.deep.equal(newTask.tags);
	});
});
describe('Your makeTaskFromObject function', function(){	
	it('is a function', function() {
		expect(Task.fromObject).to.be.a('function');
	});
	it('returns an object', function() {
		var newTask = Task.new();
		newTask.title = 'thing';
		newTask.isCompleted = null;
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);

		var makeFromObj = Task.fromObject(newTask);
		expect(makeFromObj).to.be.a('object');
	});
	it('has the same title as the newTask that is clones', function() {
		var newTask = Task.new();
		newTask.title = 'thing';
		newTask.isCompleted = null;
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);

		var makeFromObj = Task.fromObject(newTask);
		expect(makeFromObj.title).to.equal(newTask.title);
	});
	it('has the same tags as the newTask that it clones', function() {
		var newTask = Task.new();
		newTask.title = 'thing';
		newTask.isCompleted = null;
		var arr = ['not here', 'here', 'here here'];
		newTask.prototype.addTags.call(newTask, arr);

		var makeFromObj = Task.fromObject(newTask);
		expect(makeFromObj.tags).to.deep.equal(newTask.tags);
	});
});
describe('Your makeTaskFromString function', function() {
	it('is a function', function() {
		expect(Task.fromString).to.be.a('function');
	});
	it('returns an object', function() {
		var str;
		str = 'hi there #hottopic';
		var makeFromStr = Task.fromString(str);
		expect(makeFromStr).to.be.a('object');
	});
	it('correctly assigns a title', function() {
		var str;
		str = 'hi there #hottopic';
		var makeFromStr = Task.fromString(str);
		expect(makeFromStr.title).to.equal('hi there');
	});
	it('correctly assigns the tags', function() {
		var str;
		str = 'hi there #hottopic';
		var makeFromStr = Task.fromString(str);
		expect(makeFromStr.tags).to.deep.equal(['hottopic']);
	});

});
